import cheerio from "cheerio";

interface PokemonObject {
  nationalDex: number;
  name: string;
  image_url: string;
  url: string;
  type: string[];
}

const url_prefix = "https://bulbapedia.bulbagarden.net";

const ParsePokemonRow = (PokemonAttributes: string[]): PokemonObject | void => {
  let image_url = PokemonAttributes.pop();

  if (image_url === undefined) {
    console.log(`Invalid Data : ${PokemonAttributes}`);
    return;
  } else {
    if (PokemonAttributes[0] === "") {
      console.log(`Invalid Data : ${PokemonAttributes}`);
      return;
    } else {
      image_url = image_url.substr(2);
      const url = url_prefix + PokemonAttributes.pop();
      const nationalDex = Number(PokemonAttributes[1].substr(1));
      if (isNaN(nationalDex)) {
        console.log(`Invalid Data : ${PokemonAttributes}`);
        return;
      }
      const type = [PokemonAttributes[3]];
      if (PokemonAttributes[4]) {
        type.push(PokemonAttributes[4]);
      }
      const Pokemon: PokemonObject = {
        nationalDex,
        image_url,
        url,
        name: PokemonAttributes[2],
        type,
      };
      return Pokemon;
    }
  }
};

export default ParsePokemonRow;
export { PokemonObject };
