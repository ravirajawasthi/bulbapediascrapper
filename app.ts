import Axios from "axios";
import cheerio from "cheerio";
import ParsePokemonRow, { PokemonObject } from "./util/ParsePokemonRow";

const getPokemonNames = async () => {
  const url =
    "https://bulbapedia.bulbagarden.net/wiki/List_of_Pok%C3%A9mon_by_National_Pok%C3%A9dex_number";

  const { data } = await Axios.get(url);
  const $ = cheerio.load(data);
  const tables = $("h3 + table > tbody");

  const pokemons: PokemonObject[] = [];

  tables.each((index, table) => {
    const $table = $(table);
    $table
      .find("tr")
      .slice(1)
      .each((index, row) => {
        const $row = $(row);
        const columns = $row.find("td");
        let text_content: string[] = [];
        columns.each((_index, column) => {
          const $ = cheerio.load(column);
          const $column = $(column);
          text_content.push($column.text().trim());
        });
        text_content.push($($row.find("th > a")[0]).attr("href")!);
        text_content.push($($row.find("th > a > img")[0]).attr("src")!);
        const Pokemon = ParsePokemonRow(text_content);
      });
  });
};
getPokemonNames();
